/* mobile menu ====================================== */

jQuery(document).ready(function($){
	
	/* toggle nav */
	$("#menu-icon").on("click", function(){
		$(".menu-main-container").slideToggle();
		$(this).toggleClass("active");
	});

	// check cursor position
        var mouseX;
        var mouseY;
        $(document).mousemove( function(e) {
           mouseX = e.pageX; 
           mouseY = e.pageY;
        });

        // show/hide .display divs on click of imagemap, using cursor position for placement
        $('.popup').click(function(){
            var item=$(this).attr('item');
            $(".display").hide();
            $("#"+item).css({'top':mouseY - 150,'left':mouseX}).show();
            return false;
        });

        //close function for .display divs
        $('.close').click(function( event ){
            event.preventDefault();
            $(".display").hide();
            //$(".mask").hide();
        });

        $('.center').click(function( event ){
            event.preventDefault();
            $(".display").hide();
            //$(".mask").hide();
        });

   		// Intro animation, just for the sake of it
   		$(document).ready(function() {
			$(".opac").each(function (i) {
		    // store the item for use in the 'timeout' function
		    var $item = $(this);
		    // execute this function sometime later:
		    setTimeout(function() {
		      $item.animate({"opacity": 1}, 1000);
		    }, 200*i);
		    // each element should animate 200ms after the last one.
		  });
		});

        $('.open').click(function( event ){
            event.preventDefault();
            $(".title").slideDown();
            $(".open").css({'display' : "none"});
        });


    	// click on link
        $('.challenges a').on({
		    click: function(){
                // hide any popups
                $(".display").hide();
                $(".title").slideUp();
                $(".open").css({'display' : "block"});
                
		    //LOLLIPOP MARKERS opacity
		    	//set all link copy to default grey
		        $('.challenges ul li a').css({'color' : '#84868d'});
		        //setup custom attribute 'item' as variable
                var item=$(this).attr('item');
                //highlight link copy
                $(this).css({'color' : '#333e48'});
                //set all svgs back to opac 0.2
                $(".opac").css({'opacity' : 0.2});
                //set svgs that share item name as a class to opac 1
                $(".opac."+item).css({'opacity' : 1});

            //DOT INDICATORS and copy opacity
            	// reset all dot indicators to grey & remove all copy highlights
		        $('.dot').css({'fill-opacity' : 0.2});
		        $(".dot").parent().siblings("p").css({'color' : '#84868d'});
		        // set each dot that shares item name as a class to opac
                $(".dot."+item).css({'fill-opacity' : 1});
                // set each p tag to highlight
                $(".dot."+item).parent().siblings("p").css({'color' : '#333e48'});
                //scroll to anchor
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                  var target = $(this.hash);
                  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                  if (target.length) {
                    $('html,body').animate({
                      scrollTop: target.offset().top
                    }, 300);
                    return false;
                  }
                };
                console.log(item);
                return false;
		    }
		});
});

